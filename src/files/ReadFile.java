package files;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class ReadFile {
    /**
    * Informations has been taken on the website https://www.journaldev.com/867/java-read-text-file.
    * I simply google "java best way to read file".

    * To illustrate how can we read a file, we have a file with an enumerates list of animals.
    * This file is 'animals.txt' , i suggest you to check what is it written in.
    */

    /**
    * FILENAME is the path to find the file 'animals.txt'. This file is in the same folder than this .java
     */
    public static final String FILENAME = new File("src/files/animals.txt").getAbsolutePath();

    /**
     * Here is simply the method to execute the reading file.
     */

    public static void main(String[] args) throws IOException {
        ReadFile rf = new ReadFile();
        System.out.println("Reading file with FileReader :" + System.lineSeparator());
        rf.displayAnimalsWithFileReader(FILENAME);

        System.out.println(System.lineSeparator() + "Reading file with BufferReader :" + System.lineSeparator());
        rf.displayAnimalsWithBufferReader(FILENAME);

        System.out.println(System.lineSeparator() + "Reading file with Scanner :" + System.lineSeparator());
        rf.displayAnimalsWithScanner(FILENAME);
    }

    /**
    * 1st Filereader
    * Read line by line
    * FileReader doesn’t support encoding and works with the system default encoding.
    * It’s not a very efficient way of reading a text file in java.

    * -------------------------------------------------------------
    */

    public void displayAnimalsWithFileReader(String filename) throws IOException {
        File file = new File(filename);
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String line;
        while((line = br.readLine()) != null){
            //process the line
            System.out.println(line);
        }
    }

    /**
     * -------------------------------------------------------------


    * 2nd BufferReader
    * Read line by line
    * It’s good for processing the large file and it supports encoding also.
    * BufferedReader is synchronized, so read operations on a BufferedReader can safely be done from multiple threads.

    * -------------------------------------------------------------

     */

    public void displayAnimalsWithBufferReader(String filename) throws IOException {
        File file = new File(filename);
        FileInputStream fis = new FileInputStream(file);
        InputStreamReader isr = new InputStreamReader(fis);
        BufferedReader br = new BufferedReader(isr);

        String line;
        while((line = br.readLine()) != null){
            //process the line
            System.out.println(line);
        }
        br.close();
    }

    /**
    * -------------------------------------------------------------

    * 3rd Scanner
    * Read line by line
    * If you want to read file line by line or based on some java regular expression, Scanner is the class to use.

    * -------------------------------------------------------------

     */

    public void displayAnimalsWithScanner(String filename) throws IOException {
        Path path = Paths.get(filename);
        Scanner scanner = new Scanner(path);
        //read line by line
        while (scanner.hasNextLine()) {
            //process each line
            String line = scanner.nextLine();
            System.out.println(line);
        }
        scanner.close();
    }

}
