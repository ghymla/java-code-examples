package language;

class Calculator {

    // This is a static variable. It can be accessed from the class directly without having an instance.
    // static variables are often used as constants, this is why this attribute is also final.
    public static final double PI = 3.1416;

    // This is a _static method_. It can be accessed directly from the class without having an instance.
    public static double add(double a, double b) {
        return a+b;
    }
}

public class StaticMembers {

    public static void main(String[] args) {
        // Calling a static member is done with the name of the class. It can't be called with an instance.
        System.out.println(Calculator.add(Calculator.PI, 1));
    }
}